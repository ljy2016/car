# -*- coding: utf-8 -*-
# @Author  : 陈和权
# @Time    : 2019/10/19 16:08
# @File    : jt808_command.py
import binascii


jt808_command_dict={'request_video':'9101','writing_msg':'8300','call_now':'8201','send_rate':'0029','push_to_device':'8900','query_video_status':'9003',
					'video_control':'9102'}


def jt808_command(device_id, kind, param, model):
	'''
	:param device_id: 设备id
	:param kind: 下发命令
	:param param: 命令参数
	:param model:
	:return: 组包好的命令
	'''
	senddata=b'0'
	device_id = '0' + device_id
	message_id = jt808_command_dict[kind]

	if kind == 'writing_msg':  # 文本信息下发
		'''
		08:TTS语音播报
		'''
		if param == 'text':
			text = bytes.hex('$$00000001000000500110019500000000000000测试AA&&'.encode('gbk'))
			senddata = '8300' + '%04x' % int(len(text) / 2 + 1) + device_id + '0000' + message_id[-2:] + text
			return Group_Package(senddata)
		else:
			text = bytes.hex(param.encode('gbk'))
			senddata = '8300' + '%04x' % int(len(text) / 2 + 1) + device_id + '0000' + message_id[-2:] + text
			

	elif kind == 'call_now':  # 位置信息查询
		senddata = '8201' + '0000' + device_id + '0000'

		

	elif kind == '8103':  # 设置终端参数
		pass

	elif kind == 'send_rate':  # 设置定位回传间隔
		seconds = '%08x' % int(param)
		senddata = '8103' + '000a' + device_id + '0000' + '01' + '00000029' + '04' + seconds

		

	elif kind == 'push_to_device':  # 数据下行透传
		text_color = ''
		if param == 'red':
			text_color = bytes.hex('$$990000000&&'.encode('gbk'))
		elif param == 'green':
			text_color = bytes.hex('$$990000001&&'.encode('gbk'))
		elif param == 'yellow':
			text_color = bytes.hex('$$990000002&&'.encode('gbk'))

		if text_color != '':
			text = text_color
		else:
			text = bytes.hex(('$$00000001000000500110019500000000000000'+param+'AA&&').encode('gbk'))
		senddata = '8900' + '%04x' % int(len(text)) + device_id + '0000' + '41' + text
		

	elif kind == 'request_video': #实时音视频传输请求
		ip=param[0]
		port=param[1]
		camera_num=param[2]
		ip = bytes.hex(ip.encode())
		# print(ip)
		port = '%4x'%int(port)
		# print(port)
		ip_len = '%02x'%int(len(ip) / 2)
		# print(ip_len)
		senddata = '9101' + '%04x'%(int(ip_len, 16) + 8) + device_id + '0000' + ip_len + ip + port + port + camera_num + '00' + '01'
		# print(senddata)

		
	elif kind == 'query_video_status':
		senddata=message_id+'0000'+device_id+'0000'
		
	elif kind=='video_control':
		ctr_kind_dict={'close_video':'00','change_code':'01','pause_video':'02','continue_play':'03','close_chat':'04'}
		close_video_dict={'close_video_voice':'00','close_voice_only':'01','close_video_only':'02'}
		change_code_dict={'main_stream':'00','sub_stream':'01'}
		# 摄像头通道号
		camera_num=param[0]
		# 控制指令类型
		ctr_kind=ctr_kind_dict[param[1]]
		# 关闭音视频指令相关
		close_video=close_video_dict[param[2]]
		# 切换码流相关
		change_code=change_code_dict[param[3]]

		mesg_len='0004'
		senddata=message_id+'0004'+device_id+'0000'+camera_num+ctr_kind+close_video+change_code

	return Group_Package(senddata)


def Group_Package(data):
	'''
	:param data: 消息头+消息体
	:return: 转义后的数据。标识位+消息头+消息体+校验+标识位
	'''
	responsedata = Escape(data + Check(data))

	return responsedata


def Check(data):
	'''
	前一个字节与后一个字节异或
	:param data:接收的完整数据
	:return:
	'''

	n = int(len(data) / 2)
	xor = 0
	for r in range(n):  # 第一次与0异或
		xor ^= int(data[2 * r:2 * r + 2], 16)
	xor = hex(xor)
	if len(xor) != 4:
		xor = '0' + xor[2:]
	else:
		xor = xor[2:]  # 返回16进制字符串

	return xor


def Escape(data):
	'''
	下发的数据需要进行转义(标识位不需要转义)
	规则： 7d -> 7d01,  7e -> 7d02
	:param data: 需要转义的数据消息头+消息体+校验
	:return: 转移后的数据
	'''

	data = data.replace('7d', '7d01')
	data = data.replace('7e', '7d02')

	escapedata = '7e' + data + '7e'


	return binascii.unhexlify(escapedata)


# dd=jt808_command('11111111111','request_video',['139.111.222.444','13836','02'], 'model')
# print(dd)